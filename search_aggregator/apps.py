from django.apps import AppConfig


class SearchAggregatorConfig(AppConfig):
    name = 'search_aggregator'

from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'search_aggregator'

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^result', views.result, name='result')
]

from django.shortcuts import render

from .models import Product

from search_aggregator.service import search_paytm, search_shopclue, search_tatacliq


def index(request):
    # TODO: Use pagination and per page item here
    products = Product.objects.all()
    product_list = []
    for p in products:
        product_list.append(
            {
                "product_id": p.product_id,
                "product_name": p.name,
                "url": p.url,
                "image_url": p.image_url,
                "price": p.price
            }
        )
    context = {'product_list': product_list}
    return render(request, 'search_aggregator/index.html', context)


def result(request):
    post_data = request.POST
    search_string = post_data.get('search_string')
    # TODO: USE Threading or async in API calls
    products = search_paytm(search_string)
    products.extend(search_shopclue(search_string))
    products.extend(search_tatacliq(search_string))
    context = {'product_list': products}
    return render(request, 'search_aggregator/search_result.html', context)

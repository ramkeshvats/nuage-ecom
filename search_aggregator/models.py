from django.db import models
from enum import IntEnum, unique


@unique
class SourceType(IntEnum):
    PAYTM = 1
    SHOPCLUES = 2
    TATACLIQ = 3


ITEM_SOURCES = tuple((member.value, member.name) for _, member in SourceType.__members__.items())


# ITEM_SOURCES = (
#     (1, 'PAYTM'),
#     (2, 'SHOPCLUES'),
#     (3, 'TATACLIQ')
# )


class Product(models.Model):
    name = models.CharField(max_length=800, db_index=True)
    product_id = models.CharField(max_length=200)
    url = models.URLField(max_length=800, db_index=True, unique=True)
    image_url = models.URLField(max_length=800)
    price = models.FloatField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)
    source = models.IntegerField(choices=ITEM_SOURCES)





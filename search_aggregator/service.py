import requests
import json

from search_aggregator.models import Product, SourceType
from django.conf import settings


def search_paytm(search_string):
    url = settings.PAYTM_URL.format(search_string)
    try:
        response = requests.get(url)
        return prepare_paytm_result(json.loads(response.content.decode('utf-8')))
    except Exception as e:
        # TODO: Implement logging here
        print("Exception: {} occur".format(e))


def search_shopclue(search_string):
    url = settings.SHOPCLUES_URL.format(search_string)
    try:
        response = requests.get(url)
        return prepare_shopclues_result(json.loads(response.content.decode('utf-8')))
    except Exception as e:
        print("Exception: {} occur".format(e))


def search_tatacliq(search_string):
    url = settings.TATACLIQ_URL.format(search_string)
    try:
        response = requests.post(url)
        return prepare_tatacliq_result(json.loads(response.content.decode('utf-8')))
    except Exception as e:
        print("Exception: {} occur".format(e))


def prepare_paytm_result(search_result):
    products = []
    for p in search_result['grid_layout']:
        products.append(
            {
                "product_id": p.get("product_id"),
                "product_name": p.get("name"),
                "url": p.get("url"),
                "image_url": p.get("image_url"),
                "price": p.get("offer_price")
            }
        )
        product, created = Product.objects.get_or_create(
            url=p.get("url"), source=SourceType.PAYTM.value
        )
        product.price = p.get("offer_price")
        product.image_url = p.get("image_url")
        product.name = p.get("name")
        product.product_id = p.get("product_id")
        product.save()
    return products


def prepare_shopclues_result(search_result):
    products = []
    for p in search_result.get('products'):
        # TODO: Wrap this in a function
        product_url = p.get("product_url")
        if isinstance(product_url, list):
            product_url = product_url[0]
        products.append(
            {
                "product_id": p.get("product_id"),
                "product_name": p.get("product"),
                "url": product_url,
                "image_url": p.get("image_url"),
                "price": p.get("price")
            }
        )
        product, created = Product.objects.get_or_create(
            url=product_url, source=SourceType.SHOPCLUES.value
        )
        product.price = p.get("price")
        product.image_url = p.get("image_url")
        product.name = p.get("product")
        product.product_id = p.get("product_id")
        product.save()
    return products


def prepare_tatacliq_result(search_result):
    products = []
    for p in search_result.get('searchresult'):
        url = "https://www.tatacliq.com/" + p.get("productname").replace(' ', '-') + "p-" + p.get("productId")
        products.append(
            {
                "product_id": p.get("productId"),
                "product_name": p.get("productname"),
                # "url": p.get("product_url"),
                "url": url,
                "image_url": p.get("imageURL"),
                "price": p.get("sellingPrice").get('value')
            }
        )
        product, created = Product.objects.get_or_create(
            url=url, source=SourceType.TATACLIQ.value
        )
        product.price = p.get("sellingPrice").get('value')
        product.image_url = p.get("imageURL")
        product.name = p.get("productname")
        product.product_id = p.get("productId")
        product.save()
    return products
